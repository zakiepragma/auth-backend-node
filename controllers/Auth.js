import Users from "../models/UserModel.js";
import argon2 from "argon2";

export const register = async (req, res) => {
  const { name, email, password, confirmPassword, role } = req.body;

  // Memeriksa apakah password dan konfirmasi password cocok
  if (password !== confirmPassword) {
    return res
      .status(400)
      .json({ msg: "Password and confirm password do not match" });
  }

  try {
    // Membuat hash dari password yang diberikan
    const hashPassword = await argon2.hash(password);

    // Menyimpan data user ke database
    await Users.create({
      name,
      email,
      password: hashPassword,
      role,
    });

    // Mengirimkan respons berhasil
    res.status(201).json({ msg: "User register successfully" });
  } catch (error) {
    console.error(error);
    res.status(500).json({ msg: error.message });
  }
};

export const Login = async (req, res) => {
  const user = await Users.findOne({
    where: {
      email: req.body.email,
    },
  });

  if (!user) return res.status(404).json({ msg: "User not found" });

  const match = await argon2.verify(user.password, req.body.password);

  if (!match) return res.status(400).json({ msg: "Wrong password" });

  req.session.userId = user.uuid;

  const uuid = user.uuid;
  const name = user.name;
  const email = user.email;
  const role = user.role;

  res.status(200).json({ uuid, name, email, role });
};

export const Me = async (req, res) => {
  if (!req.session.userId) {
    return res.status(401).json({ msg: "Mohon login ke akun anda" });
  }

  const user = await Users.findOne({
    attributes: ["uuid", "name", "email", "role"],
    where: {
      uuid: req.session.userId,
    },
  });

  if (!user) return res.status(404).json({ msg: "User not found" });

  res.status(200).json(user);
};

export const LogOut = async (req, res) => {
  req.session.destroy((err) => {
    if (err) return res.status(400).json({ msg: "Tidak dapat logout" });

    res.status(200).json({ msg: "Anda telah logout" });
  });
};
